import machine
from umqttsimple import MQTTClient
import time

led = machine.Pin(2, machine.Pin.OUT)
freezer_door = machine.Pin(23, machine.Pin.IN, machine.Pin.PULL_UP)
fridge_door = machine.Pin(22, machine.Pin.IN, machine.Pin.PULL_UP)


def connect():
    global client_id, mqtt_server
    client = MQTTClient("fridge_esp", mqtt_server)
    client.connect()
    print('Connected to %s MQTT broker' % mqtt_server)
    return client


def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()


try:
    client = connect()
except OSError as e:
    restart_and_reconnect()


def is_fridge_open():
    return fridge_door.value() == 1


def is_freezer_open():
    return freezer_door.value() == 1


freezer_open = False
fridge_open = False


def check_fridge_and_notify():
    global freezer_open, fridge_open
    if is_fridge_open():
        if not fridge_open:
            client.publish(b'FRIDGE_DOOR', b'1')
            led.value(1)
            fridge_open = True
    elif fridge_open:
        client.publish(b'FRIDGE_DOOR', b'0')
        led.value(0)
        fridge_open = False

    if is_freezer_open():
        if not freezer_open:
            client.publish(b'FREEZER_DOOR', b'1')
            led.value(1)
            freezer_open = True
    elif freezer_open:
        client.publish(b'FREEZER_DOOR', b'0')
        led.value(0)
        freezer_open = False


while True:
    try:
        check_fridge_and_notify()
    except OSError as e:
        restart_and_reconnect()
    time.sleep(1)
