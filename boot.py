import time
import ubinascii
import machine
import micropython
import network
import esp
import gc

esp.osdebug(None)
gc.collect()

ssid = 'SSID'
password = 'PASSWORD'
mqtt_server = '192.168.178.39'
client_id = ubinascii.hexlify(machine.unique_id())

last_message = 0
message_interval = 5
counter = 0

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while not station.isconnected():
    pass

print('Connection successful')
print(station.ifconfig())
